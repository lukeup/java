
public class Cat extends Animal{
	public Cat() {
		
	}
	  
	public String makeSound() {
		return "meow";
	}
	public static void main(String[] args) {
		Animal piko = new Dog();
		Animal peper = new Cat();
		
		Animal[] theAnimals = new Animal[10];
		
		theAnimals[0] = piko;
		theAnimals[1] = peper;
		
		System.out.println("Piko says " + theAnimals[0].makeSound());
		System.out.println("Peper says " + theAnimals[1].makeSound());
		
		speakAnimal(piko);
	}
}

