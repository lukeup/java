import java.util.Scanner;
import java.util.*;

public class Animal {
	// static - will be shared by every Animal obj that is created
	// final - means that will be constant and can't be changed
	public static final double FAVNUMBER = 1.1802;

	private String name;
	private int weight;
	private boolean hasOwner = false;
	private byte age;
	private long uniqueId;
	private char favouriteChar;
	private double speed;
	private float height;

	protected static int numberOfAnimals = 0;

	// to get data from the keyboard
	static Scanner userinput = new Scanner(System.in);

	// super() calls whatever the super class was for this Animal to be executed
	// Animal is superClass for Dog and Cat
	public Animal() {
		// super();
		numberOfAnimals++;

		int sumOfNumbers = 5 + 1;
		System.out.println("5 + 1 = " + sumOfNumbers);

		int diffOfNumbers = 5 - 1;
		System.out.println("5 - 1 = " + diffOfNumbers);

		int multOfNumbers = 5 * 1;
		System.out.println("5 * 1 = " + multOfNumbers);

		int divOfNumbers = 5 / 1;
		System.out.println("5 / 1 = " + divOfNumbers);

		int modOfNumbers = 5 % 3;
		System.out.println("5 % 3 = " + modOfNumbers);

		System.out.print("Enter the name: \n");

		// hasNextLine = string, hasNextInt
		if (userinput.hasNextLine()) {
			this.setName(userinput.nextLine());
		}
		this.setFavouriteChar();
		this.setUniqueId();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public boolean isHasOwner() {
		return hasOwner;
	}

	public void setHasOwner(boolean hasOwner) {
		this.hasOwner = hasOwner;
	}

	public byte getAge() {
		return age;
	}

	public void setAge(byte age) {
		this.age = age;
	}

	public long getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(long uniqueId) {
		this.uniqueId = uniqueId;
		System.out.println("Unique ID set to: " + this.uniqueId);
	}

	public void setUniqueId() {
		long minNumber = 1;
		long maxNumber = 1000000;
		// casting (long) because Math.random() gets double
		this.uniqueId = minNumber + (long) (Math.random() * ((maxNumber - minNumber) + 1));

		String stringNumber = Long.toString(maxNumber);

	}

	public char getFavouriteChar() {
		return favouriteChar;
	}

	public void setFavouriteChar(char favouriteChar) {
		this.favouriteChar = favouriteChar;
	}

	public void setFavouriteChar() {
		int randomNumber = (int) (Math.random() * 126) + 1;

		this.favouriteChar = (char) randomNumber;

		if (randomNumber == 32) {
			System.out.println("Favourite character set to Space");
		} else if (randomNumber == 10) {
			System.out.println("Favourite character set to Newline");
		} else {
			System.out.println("Favourite character set to " + this.favouriteChar);
		}
		// numbers are from char codes
		if ((randomNumber > 97) && (randomNumber < 122)) {
			System.out.println("Favourite character is a lowercase letter");
		}

		if (((randomNumber > 97) && (randomNumber < 122)) || ((randomNumber > 97) && (randomNumber < 122))) {
			System.out.println("Favourite character is a letter");
		}

		int whichIsBigger = (50 > randomNumber) ? 50 : randomNumber;
System.out.println(whichIsBigger);
		
		switch (randomNumber) {
		case 8:
			System.out.println("Favourite character set to backspace");
			break;
		case 10:
		case 11:
		case 12:
			System.out.println("Favourite char");
		default:
			System.out.println("Favourite character set to default");
			break;

		}

	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	protected static void countTo(int startingNumber) {
		for (int i = startingNumber; i <= 100; i++) {
			if (i == 90)
				continue;

			System.out.println(i);
		}
	}

	protected static String printNumbers(int maxNumbers) {
		int i = 1;
		while (i < (maxNumbers / 2)) {
			System.out.println(i);
			i++;

			if (i == (maxNumbers / 2))
				break;

		}
		// Animal because static calss for class
		Animal.countTo(maxNumbers / 2);
		return "End of printNumbers";
	}

	protected static void guessMyNumber() {
		int number;

		do {
			System.out.println("Guess Number up to 100");

			while (!userinput.hasNextInt()) {
				String numberEntered = userinput.next();
				System.out.printf("%s is not a number\n", numberEntered);

			}
			number = userinput.nextInt();
		} while (number != 50);
		System.out.println("You entered number not char!");
	}
	
	public String makeSound() {
		return "Grrr";
	}
	
	public static void speakAnimal(Animal randAnimal) {
		System.out.println("Animal says " + randAnimal.makeSound());
	}

	public static void main(String[] args) {
		Animal theAnimal = new Animal();
		Animal.countTo(10);
		
		int[] favouriteNumber;
		favouriteNumber = new int[20];
		
		favouriteNumber[0] = 100;
		
		String[] stringArray = {"Random","Words","Here"};
		for(String word : stringArray)
		{
			System.out.println(word);
		}
		
		
	}

}
